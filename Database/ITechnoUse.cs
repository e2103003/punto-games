﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Punto.Database
{
    /// <summary>
    /// Interface permettant de gérer les données dans les bases de données
    /// </summary>
    public interface ITechnoUse
    {
        List<Player> LoadPlayersFromDatabaseAsync();

        void AddPlayerToDatabase(Player newPlayer);

        void UpdatePlayerInDatabase(Player playerToUpdate);

        void DeletePlayerFromDatabase(int playerId);





    }
}
