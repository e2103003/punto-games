﻿using System;
using System.Collections.Generic;
using System.Linq;
using Punto.Database;
using System.Windows;
using System.Threading.Tasks;
using Neo4j.Driver;
using Google.Protobuf.WellKnownTypes;
using MySqlX.XDevAPI.Common;
using Neo4jClient;
using System.Xml.Linq;
using System.Windows.Documents;

namespace Punto
{
    internal class Neo4jUse //: ITechnoUse
    {

        private readonly IDriver _driver;
        private IGraphClient graphClient;

        public Neo4jUse()
        {
            try
            {
                // Remplacez les informations de connexion par celles de votre base de données Neo4j
                var uri = new Uri("bolt://localhost:7687/");


                _driver = GraphDatabase.Driver(uri);
                _driver.VerifyConnectivityAsync().Wait();

                

            }
            catch (Exception e)
            {
                MessageBox.Show($"Erreur connexion à la base de données Neo4j : {e}");
            }
        }

        public async Task<List<Player>> LoadPlayersFromDatabaseAsync()
        {
            List<Player> ret = new List<Player>();

            try
            {
                using (var session = _driver.AsyncSession())
                {
                    // Exécutez une requête de lecture pour récupérer le joueur par son nom
                    var result = await session.ReadTransactionAsync(async tx =>
                    {
                        var cursor = await tx.RunAsync("MATCH (p:Player) RETURN p");
                        return await cursor.ToListAsync();
                    });

                    // On traite le résultat pour pouvoir convertir les données en liste de joueurs
                    var records = result.Select(r => r["p"].As<INode>()).ToList();
                    var players = records.Select(r => new Player
                    {
                        Id = Convert.ToInt32(r.Id),
                        Name = r.Properties["name"].ToString(),
                        Color = r.Properties["color"].ToString(),
                        Wins = Convert.ToInt32(r.Properties["wins"]),
                        LastWin = r.Properties["lastWin"].ToString()
                    }).ToList();

                    ret = players;

                }
            }
            catch(Exception e)
            {
                MessageBox.Show($"Erreur connexion à la base de données Neo4j : {e}");
            }

            return ret;

        }

        public async Task AddPlayerToDatabaseAsync(Player newPlayer)
        {
            try
            {
                using (var sessions = _driver.AsyncSession())
                {
                    await sessions.WriteTransactionAsync(tx => tx.RunAsync(
                    "CREATE (p:Player {name: $Name, color: $Color, wins: $Wins, lastWin: $LastWin})",
                    new { newPlayer.Name, newPlayer.Color, newPlayer.Wins, newPlayer.LastWin }
                ));
                }

            } catch(Exception e)
            {
                MessageBox.Show($"Erreur connexion à la base de données Neo4j : {e}");
            }
            
        }

        public async Task<int> UpdatePlayerInDatabaseAsync(Player updatedPlayer)
        {
            using (var session = _driver.AsyncSession())
            {
                // On update en fonction de l'ID du Player et de la meme facon que MySQL : UPDATE Player SET Name = @Name, Color = @Color, Wins = @Wins, LastWin = @LastWin WHERE Id = @Id
                var result = await session.WriteTransactionAsync(tx => tx.RunAsync(
                    "MATCH (player:Player) " +
                    "WHERE id(player) = $Id " +
                    "SET player.name = $Name, player.color = $Color, player.wins = $Wins , player.lastWin = $LastWin " +
                    "RETURN player",
                    new {updatedPlayer.Id, updatedPlayer.Name, updatedPlayer.Color, updatedPlayer.Wins, updatedPlayer.LastWin}
                ));

                

                return updatedPlayer.Id;
                
            }
            
        }

        public async Task<int> DeletePlayerFromDatabaseAsync(int playerId)
        {
            using (var session = _driver.AsyncSession())
            {
                // On supprime en fonction de l'ID du Player
                await session.WriteTransactionAsync(tx => tx.RunAsync(
                    "MATCH (p:Player) WHERE id(p) = $Id DETACH DELETE p",
                    new { Id = playerId }
              ));

            }
            return playerId;
        }

       
        public void AddVictoryToDatabase(Player winner)
        {
            using (var session = _driver.AsyncSession())
            {
                // On update en fonction de l'ID du Player et de la meme facon que MySQL : UPDATE Player SET Name = @Name, Color = @Color, Wins = @Wins, LastWin = @LastWin WHERE Id = @Id
                session.WriteTransactionAsync(tx => tx.RunAsync(
                    "MATCH (p:Player {name: $Name, color: $Color, wins: $Wins, lastWin: $LastWin}) WHERE p.id = $Id",
                    new { winner.Name, winner.Color, winner.Wins, winner.LastWin, winner.Id }
                ));


            }
        }


        public async Task CreateRelationshipAsync(List<Player> players)
        {
            using (var session = _driver.AsyncSession())
            {
                // On créer une relation "PLAYED_TOGETHER" entre les joueurs qui ont joué ensemble
                await session.WriteTransactionAsync(tx => tx.RunAsync(
                    "MATCH (p1:Player), (p2:Player) " +
                    "WHERE id(p1) = $Id1 AND id(p2) = $Id2 " +
                    "CREATE (p1)-[r:PLAYED_TOGETHER]->(p2) " +
                    "RETURN type(r)",
                new { Id1 = players[0].Id, Id2 = players[1].Id }
                ));
                
            }
        }

    }
}
